class Person < ApplicationRecord
  # attr_accessor :full_name, :birth_date, :cpf, :home_phone, :home_phone, :rg
  belongs_to :address
  accepts_nested_attributes_for :address

  has_one :user

  scope :filter_by_full_name, lambda { |keyword| where("lower(full_name) LIKE ?", "%#{keyword.downcase}%") }
  scope :filter_by_cpf, lambda { |keyword| where("lower(cpf) LIKE ?", "%#{keyword.downcase}%") }
end
